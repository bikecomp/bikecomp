#define TINY_GSM_MODEM_SIM7600

#define SerialMon Serial
#define SerialAT Serial1
#define TINY_GSM_DEBUG SerialMon

#define UART_BAUD 115200
#define PIN_TX 26
#define PIN_RX 27
#define PWR_PIN 4
#define BAT_EN 12
#define RESET 5

#define SD_MISO 2
#define SD_MOSI 15
#define SD_SCK 14
#define SD_CS 13

#include "TinyGsmClient.h"
#include "PubSubClient.h"
#include "Ticker.h"
#include "MS5611.h"
#include "FS.h"
#include "SPI.h"
#include "SD.h"
#include "WiFi.h"

#include "pass.hpp"
#include "mpu.hpp"


#ifdef DUMP_AT_COMMANDS
#include <StreamDebugger.h>
StreamDebugger debugger(SerialAT, SerialMon);
TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif
TinyGsmClient client(modem);
PubSubClient mqtt(client);
MS5611 ms5611(0x77);
SPIClass spi = SPIClass(VSPI);

uint32_t lastReconnectAttempt = 0;

bool isSecurityMode = false;
bool isStolen = false;

const char *csvPath = "/bikecomp.csv";

int dormantSum;

// MQTT
const char *mqttBroker = "s.rflx.xyz";
const char *topicLat = "bikecomp/lat";
const char *topicLon = "bikecomp/lon";
const char *topicSpeed = "bikecomp/spd";
const char *topicAlt = "bikecomp/alt";
const char *topicAtm = "bikecomp/atm";
const char *topicStolen = "bikecomp/stl";
const char *topicWifi = "bikecomp/wifi";

const char *wifiSsid = "bikecomp";

void sdcardInit() {
    spi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);

    if (!SD.begin(SD_CS, spi, 80000000)){
        Serial.println("Card Mount Failed");
        return;
    }
    uint8_t cardType = SD.cardType();

    if (cardType == CARD_NONE){
        Serial.println("No SD card attached");
        return;
    }
}

void sdWrite(fs::FS &fs, const char *path, const char *message) {
    File file = fs.open(path, FILE_WRITE);
    if (!file) {
        Serial.println("Failed to open file for writing");
        return;
    }
    if (!file.print(message)) {
        Serial.println("Write failed");
    }
    file.close();
}

void sdAppend(fs::FS &fs, const char *path, const char *message) {
    File file = fs.open(path, FILE_APPEND);
    if (!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if (!file.print(message)){
        Serial.println("Message appended");
        Serial.println("Append failed");
    }
    file.close();
}

boolean mqttConnect()
{
    SerialMon.print("Connecting to ");
    SerialMon.print(mqttBroker);

    boolean status = mqtt.connect(mqttUsername, mqttUsername, mqttPassword);

    if (!status) {
        SerialMon.println(" fail");
        return false;
    }

    SerialMon.println(" success");
    mqtt.subscribe(topicAtm);
    mqtt.subscribe(topicWifi);
    return mqtt.connected();
}

void mqttCallback(char *topic, byte *payload, unsigned int len)
{
    SerialMon.print("A message has arrived [");
    SerialMon.print(topic);
    SerialMon.print("]: ");

    String message = "";

    for (int i = 0; i < len; i++) {
        message += (char)payload[i];
    }

    SerialMon.println(message);

    if (String(topic) == topicAtm) {
        if (message == "1") {
            setupMPU();
            dormantSum = 0;
            recordAccelRegisters();
            for (long accel : accels) {
                dormantSum += abs(accel);
            }
            Serial.println(dormantSum);
            isSecurityMode = true;

            SerialMon.println("Security mode has been enabled");
        } else if (message == "0") {
            isSecurityMode = false;
            isStolen = false;
            SerialMon.println("Normal mode has been enabled");
        }
    }

    if (String(topic) == topicWifi) {
        if (message == "false") {
            if (WiFi.softAP(wifiSsid)) {
                SerialMon.println("WiFi has been successfully enabled");
            } else {
                SerialMon.println("Cannot enable WiFi");
            }

        } else if (message == "true") {
            if (WiFi.softAPdisconnect(true)) {
                SerialMon.println("WiFi has been successfully disabled");
            } else {
                SerialMon.println("Cannot disable WiFi");
            }
        }
    }
}

void checkInternetConnection() {
    if (!modem.isNetworkConnected()) {
        SerialMon.println("The network has been disconnected");
        if (!modem.waitForNetwork(180000L, true)) {
            SerialMon.println(" fail");
            delay(10000);
            return;
        }
        if (modem.isNetworkConnected()) {
            SerialMon.println("The network has been re-connected");
        }

        if (!modem.isGprsConnected()) {
            SerialMon.println("GPRS has been disconnected!");
            SerialMon.print(F("Connecting to the Internet"));
            if (!modem.gprsConnect("")) {
                SerialMon.println(" fail");
                delay(10000);
                return;
            }
            if (modem.isGprsConnected()) {
                SerialMon.println("GPRS has been reconnected");
            }
        }
    }

    if (!mqtt.connected()) {
        SerialMon.println("Trying to connect to the MQTT broker");
        uint32_t t = millis();
        if (t - lastReconnectAttempt > 10000L) {
            lastReconnectAttempt = t;
            if (mqttConnect()) {
                lastReconnectAttempt = 0;
            }
        }
        delay(100);
        return;
    }
}

void securityMode() {
    if (isStolen) {
        mqtt.publish(topicLat, "65.020642");
        mqtt.publish(topicLon, "35.711219");

        delay(1000);
    }
    else {
        recordAccelRegisters();
        int currentSum = 0;
        for (long accel : accels) {
            currentSum += abs(accel);
        }

        Serial.println(abs(dormantSum - currentSum));

        if (abs(dormantSum - currentSum) > 1000) {
            isStolen = true;
            mqtt.publish(topicStolen, "1");
        }

        delay(100);
    }
}

void normalMode() {
    ms5611.read();
    float temp = ms5611.getTemperature();
    float pressure = ms5611.getPressure();
    int altitude = ((pow((1013.25 / pressure), (1 / 5.257)) - 1) * (temp + 273.15)) / (0.0065);
    char altitudeString[5];
    itoa(altitude, altitudeString, 10);

    mqtt.publish(topicSpeed, "1");
    mqtt.publish(topicAlt, altitudeString);

    sdAppend(SD, csvPath, ("\n1," + String(altitude)).c_str());

    delay(1000);
}

void setup()
{
    if (!ms5611.begin()) {
        Serial.println("Can't find MS5611!");
        return;
    }

    sdcardInit();
    sdWrite(SD, csvPath, "speed,altitude");

    Serial.begin(115200);
    delay(10);

    pinMode(BAT_EN, OUTPUT);
    digitalWrite(BAT_EN, HIGH);

    pinMode(RESET, OUTPUT);
    digitalWrite(RESET, LOW);
    delay(100);
    digitalWrite(RESET, HIGH);
    delay(3000);
    digitalWrite(RESET, LOW);

    pinMode(PWR_PIN, OUTPUT);
    digitalWrite(PWR_PIN, LOW);
    delay(100);
    digitalWrite(PWR_PIN, HIGH);
    delay(1000);
    digitalWrite(PWR_PIN, LOW);

    Serial.println("\nLoading...");

    delay(10000);

    SerialAT.begin(UART_BAUD, SERIAL_8N1, PIN_RX, PIN_TX);

    DBG("Initializing modem...");
    if (!modem.init()) {
        DBG("Failed to restart the modem, delaying 10s and retrying");
        return;
    }

    SerialMon.print("Connecting to the Internet...");
    if (!modem.waitForNetwork()) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" success");

    if (modem.isNetworkConnected()) {
        SerialMon.println("The network has been connected");
    }

    SerialMon.print(F("Connecting to GPRS"));
    if (!modem.gprsConnect("")) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" success");

    if (modem.isGprsConnected()) {
        SerialMon.println("GPRS has been connected");
    }

    mqtt.setServer(mqttBroker, 1883);
    mqtt.setCallback(mqttCallback);
}

void loop()
{
    checkInternetConnection();

    isSecurityMode ? securityMode() : normalMode();

    mqtt.loop();
}
